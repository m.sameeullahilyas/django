# This file is used to define the model of the document of index of elasticsearch. Ngram filter is used so that a type of fuzzy search is possible
from elasticsearch_dsl import  analyzer, tokenizer,Keyword
from project.models import tweets
from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
# Ngram analyzer for each document
my_analyzer = analyzer('my_analyzer',
    tokenizer=tokenizer('trigram', 'edge_ngram', min_gram=3, max_gram=20),
    filter=['lowercase'])


@registry.register_document
class TweetsDocument(Document):
    Project_id = fields.IntegerField()
    text = fields.TextField(analyzer=my_analyzer)
    cleaned_text=fields.KeywordField()
    hashtags = fields.TextField(analyzer=my_analyzer,fields={'raw': Keyword(multi=True)},multi=True)
    mentions_screen_name = fields.TextField(analyzer=my_analyzer,fields={'raw': Keyword(multi=True)},multi=True)
    quoted_screen_name=fields.TextField(analyzer=my_analyzer,fields={'raw': Keyword()})
    screen_name = fields.TextField(analyzer=my_analyzer,fields={'raw': Keyword()})
    retweet_screen_name = fields.TextField(analyzer=my_analyzer,fields={'raw': Keyword()})
    status_id = fields.KeywordField()
    profile_image_url_https=fields.KeywordField()
    probability=fields.FloatField()
    id_str=fields.KeywordField()
    created_at=fields.DateField()
    timestamp_ms=fields.DoubleField()
    is_retweet=fields.BooleanField()
    is_quote=fields.BooleanField()
    retweet_count=fields.IntegerField()
    sentiment_analysis=fields.KeywordField()
    type=fields.KeywordField()
    coordinates=fields.FloatField(multi=True)
    class Index:
        # Name of the Elasticsearch index
        name = 'tweets'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 5,
                    'number_of_replicas': 0}
    class Django:
       model = tweets



