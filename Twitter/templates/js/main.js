
(function ($) {
    "use strict";

    /*==================================================================
    [ Focus Contact2 ]*/
    $('.input100').each(function(){
        $(this).on('blur', function(){
            if($(this).val().trim() != "") {
                $(this).addClass('has-val');
            }
            else {
                $(this).removeClass('has-val');
            }
        })    
    })

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(e){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
			
        }
		 
	    return check;
	
	});


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
		 
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
				$(input).parent().attr("data-validate","Email Format is wrong");
				document.getElementById('aler').style.display='';
                return false;
            }
        }
       
       if($(input).val().trim() == ''){
		   if(input.name=="Captcha"){
			   		if(grecaptcha.getResponse()==""){
			document.getElementById("recaptcha").parentElement.classList.add("alert-validate");
			document.getElementById('aler').style.display='';
            return false
		}
		else{
			document.getElementById("recaptcha").parentElement.classList.remove("alert-validate");
			
		}

		   }
		   else{
			$(input).parent().attr("data-validate",input.name+" is required")
			document.getElementById('aler').style.display='';              
			  return false;
		   }
		  
	   }
       
		
		if (((input.name).includes("Name")||input.name=='Username') && input.value.length>0 && !(/^[A-Z0-9a-z\s\u0600-\u06FF]+$/.test(input.value))) {
		$(input).parent().attr("data-validate",input.name+" must contain only Alphabets")
				document.getElementById('aler').style.display='';
				return false
			}
			
			if (input.name=='Phone'  && input.value.length>0&& !(/^\d+$/.test(input.value))) {
		
				$(input).parent().attr("data-validate",input.name+" must contain only digits")
				document.getElementById('aler').style.display='';
				return false

			}
	        if(input.name=='Start-Date'  && input.value.length>0 && document.getElementById('enddate').value.length>0){
                 if(input.value>=document.getElementById('enddate').value){
				$(input).parent().attr("data-validate",input.name+" must be less than End Date")
				document.getElementById('aler').style.display='';
				return false


                 }

	        }

			if (input.name=='Password') {
			if(input.value.length<8){
				$(input).parent().attr("data-validate",input.name+" must be of length 8 or more")
				document.getElementById('aler').style.display='';
				return false
			}
			
			if(!(/[a-z]/.test(input.value))){
				$(input).parent().attr("data-validate",input.name+" must contain atleast one small case letter")
				document.getElementById('aler').style.display='';
				return false
			}
			if((!/[A-Z]/.test(input.value))){
				$(input).parent().attr("data-validate",input.name+" must contain atleast upper case letter")
				document.getElementById('aler').style.display='';
				return false
			}
				if(!(/\d/.test(input.value))){
				$(input).parent().attr("data-validate",input.name+" must contain atleast one digit")
				document.getElementById('aler').style.display='';
				return false
			}
						if(!(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(input.value))){
				$(input).parent().attr("data-validate",input.name+" must contain atleast one special character")
				document.getElementById('aler').style.display='';
				return false
			}
			}

			}

    function showValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    

})(jQuery);
