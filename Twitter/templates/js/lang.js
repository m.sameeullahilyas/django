
var MLstrings = [
   {
    English:'Sentiment Trend',
    Arabic:'الخط الزمني للمشاعر'
   },
   { 
    English:'Early Adopters',
    Arabic:'أوائل المغردین'

   },

   {
      English:"Persian",
      Arabic:"اللغة الفارسية",
   },
   {
     English:"Start Date",
     Arabic:'تاريخ البدء'

   },

   {
      English:"Turkish",
      Arabic:"اللغة التركية",
   },

   {
    English:"Top 10 Mentions",
    Arabic:"أبرز 10 مغردین مذكورین"

   },

   {
    English:"Positive",
    Arabic:"ایجابي",

    },
    {

    English:"Negative",
    Arabic:"سلبي",
    },
{
   English:"Neutral",
   Arabic:"محاید"

},

   {
    English:"Top 10 Users",
    Arabic:"أبرز 10 مغردین"

   },
   {
    English:"Top 20 Hashtags",
    Arabic:"أبرز 20 أوسمة"

   },
   {
     English:"Tweets Timeline",
     Arabic:"الخط الزمني للتغریدات"
   },

   {
     English:"Search",
     Arabic:"بحث",
   },

   {
     English:"Reset Analysis",
     Arabic:"إعادة تعیین التحلیل",
   },
 
  {
     English:"Tweets Timeline",
     Arabic:"الخط الزمني للتغریدات",
   },
 {
     English:"Network Analysis",
     Arabic:"التحلیل الشبكي",
   },
 {
     English:"Previous",
     Arabic:"السابق",
   },

 {
     English:"Next",
     Arabic:"التالي",
   },

 {
     English:"Bot accounts",
     Arabic:"الحسابات الوهمیة",
   },
 {
     English:"Geo Location",
     Arabic:"الموقع الجغرافي",
   },
 {
     English:"Hashtags",
     Arabic:"الأوسمة",
   },
 {
     English:"Mentions",
     Arabic:"المغردین المذكورین",
   },
 {
     English:"Users",
     Arabic:"المغردین",
   },

 {
     English:"Tweets Feed",
     Arabic:"التغریدات",
   },
 {
     English:"All",
     Arabic:"الكل",
   },
 {
     English:"Links",
     Arabic:"الروابط",
   },
 {
     English:"Photos",
     Arabic:"الصور",
   },
 {
     English:"Videos",
     Arabic:"الفیدیو",
   },
 {
     English:"Others",
     Arabic:"أخرى",
   },
   {
     English:"Sentiments",
     Arabic:"تحلیل المشاعر",
   },
 {
     English:"Word Cloud",
     Arabic:"الكلمات السحابیة",
   },
 {
     English:"Word Association",
     Arabic:"سیاق الكلام"
   },

    {
       English:"Name",
       Arabic:"اسم"

    },
    {
       English:"Save",
       Arabic:"حفظ"

    },
 {
       English:"Back",
       Arabic:"عودة"

    },
 {
       English:"Delete Project",
       Arabic:"حذف المشاریع"
    },

    {
       English:"Save and Add Another",
       Arabic:"حفظ وإضافة آخر"

    },
    {
       English:"Save and Continue Editing",
       Arabic:"حفظ ومتابعة التحرير"

    },

    {
       English:"Cancel",
       Arabic:"إلغاء"

    },
   {
       English:"Location",
       Arabic:"موقعك"

    },
   {
       English:"Keywords File",
       Arabic:"ملف الكلمات الرئيسية"

    },
   {
       English:"Keywords",
       Arabic:"الكلمات الدالة"

    },
   {
       English:"English",
       Arabic:"الإنجليزية"

    },
   {
       English:"Arabic",
       Arabic:"عربى"

    },
	{
       English:"Language",
       Arabic:"لغة"

    },
	{
       English:"End Date",
       Arabic:"تاريخ الانتهاء"

    },

    {
        English: "Projects ",
        Arabic: "المشاریع",
    },
    {
       English:"Name",
      Arabic:"اسم",
    
    },
    {
        English: "Projects",
	Arabic: "مشاریع",
    },
    {
        English: "Project",
	Arabic:"اسم المشروع",
    },
    {
        English: "Id",
	Arabic:"رقم",
    },
    {
        English: "Stream",
	Arabic:"حالة المشروع",
    },
    {
        English: "Tweets Stream",
	Arabic:"إیقاف/تشغیل المشروع",
    },
    {
        English: "Start Dates",
	Arabic: "تاريخ البدء",
    },
    {
        English: "Tweet Count",
	Arabic:  "عدد التغریدات",
    },
    {
        English: "Edit Project",
	Arabic:  "تعدیل المشروع",
    },
    {
        English: "Download Data",
	Arabic:  "تحمیل البیانات",
    },
    {
        English: "Operations",
	Arabic:  "العملیات",
    },

    {
        English: "Create New Project",
	Arabic:  "انشاء مشروع جدید",
    },

    {
        English: "Account Management",
	Arabic:  "إدارة الحساب",
    },

    {
        English: "Logout",
	Arabic:  "تسجیل الخروج",
    },

];

var mlrLangInUse;

var mlr = function({
    dropID = "mbPOCControlsLangDrop",
    stringAttribute = "data-mlr-text",
    chosenLang = "Arabic",
    mLstrings = MLstrings,
    countryCodes = false,
    countryCodeData = [],
} = {}) {
    const root = document.documentElement;

    var listOfLanguages = Object.keys(mLstrings[0]);
    mlrLangInUse = chosenLang;

    (function createMLDrop() {
        var mbPOCControlsLangDrop = parent.document.getElementById(dropID);
        // Reset the menu
        mbPOCControlsLangDrop.innerHTML = "";
        // Now build the options
        listOfLanguages.forEach((lang, langidx) => {
            let HTMLoption = document.createElement("option");
            HTMLoption.value = lang;
            HTMLoption.textContent = lang;
            mbPOCControlsLangDrop.appendChild(HTMLoption);
            if (lang === chosenLang) {
                mbPOCControlsLangDrop.value = lang;
            }
        });
        mbPOCControlsLangDrop.addEventListener("change", function(e) {
            mlrLangInUse = mbPOCControlsLangDrop[mbPOCControlsLangDrop.selectedIndex].value;
            resolveAllMLStrings();
            
	    search=document.getElementById("search")
	   if(search){
search.placeholder= resolveMLString( search.placeholder, mLstrings)
}
            // Here we update the 2-digit lang attribute if required
        });
    })();


    function resolveAllMLStrings() {
        let stringsToBeResolved = document.querySelectorAll(`[${stringAttribute}]`);
        stringsToBeResolved.forEach(stringToBeResolved => {
            let originaltextContent = stringToBeResolved.textContent;
            let resolvedText = resolveMLString(originaltextContent, mLstrings);
            stringToBeResolved.textContent = resolvedText;

        });
    }
};

function resolveMLString(stringToBeResolved, mLstrings) {
    var matchingStringIndex = mLstrings.find(function(stringObj) {
        // Create an array of the objects values:
        let stringValues = Object.values(stringObj);
        // Now return if we can find that string anywhere in there
        return stringValues.includes(stringToBeResolved);
    });
    if (matchingStringIndex) {
        return matchingStringIndex[mlrLangInUse];
    } else {
        // If we don't have a match in our language strings, return the original
        return stringToBeResolved;
    }
}

mlr({
    dropID: "mbPOCControlsLangDrop",
    stringAttribute: "data-mlr-text",
    chosenLang: "Arabic",
    mLstrings: MLstrings,
});

