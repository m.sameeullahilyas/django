"""Twitter URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,re_path
from login import views
from project import views as v
from visualize import views as v1
# maps the urls to the functions
urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^start/(?P<id>[0-9]+$)', v.start),
    re_path(r'^progress/(?P<id>[0-9]+)/(?P<type>[a-z]+)$', v.prog),
    re_path(r'^create_project', v.create_project),
    re_path(r'^stop/(?P<id>[0-9]+$)', v.stop_project),
    re_path(r'^downloadcsv/(?P<id>[0-9]+)/(?P<name>.*)$', v.downloadcsv),
    re_path(r'^downloadjson/(?P<id>[0-9]+)/(?P<name>.*)$', v.downloadjson),
    re_path(r'^search/(?P<id>[0-9]+$)', v1.search),
    re_path(r'^delete/(?P<id>[0-9]+$)', v.delete),
    re_path(r'^delete_project/(?P<id>[0-9]+$)', v.delete_project),
    re_path(r'^edit/(?P<id>[0-9]+$)', v.edit),
    re_path(r'^account_edit/', v.account_edit),
    re_path(r'^(?P<id>[0-9]+$)', v1.visualize),
    re_path(r'^(?P<id>[0-9]+)/timeline/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.timeline),
    re_path(r'^(?P<id>[0-9]+)/timeline_data/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.timeline_data),
    re_path(r'^(?P<id>[0-9]+)/account/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.account),
    re_path(r'^(?P<id>[0-9]+)/account_data/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<skip>[0-9]+)$', v1.account_data),
        re_path(r'^(?P<id>[0-9]+)/adopter/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.adopter),
    re_path(r'^(?P<id>[0-9]+)/adopter_data/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<skip>[0-9]+)$', v1.adopter_data),
    re_path(r'^(?P<id>[0-9]+)/map/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.map),
    re_path(r'^(?P<id>[0-9]+)/map_data/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<skip>[0-9]+)/$', v1.map_data),
    re_path(r'^(?P<id>[0-9]+)/network/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<skip>[0-9]+)/$', v1.network),
    re_path(r'^(?P<id>[0-9]+)/hashtags/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.hashtags),
    re_path(r'^(?P<id>[0-9]+)/sentiment_trend/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<interval>[0-9a-zA-Z]+)/$', v1.sentiment_trend),
    re_path(r'^(?P<id>[0-9]+)/screen_name/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.screen_name),
    re_path(r'^(?P<id>[0-9]+)/mentions_screen_name/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.mentions_screen_name),
    re_path(r'^(?P<id>[0-9]+)/sentiments/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.sentiments),
    re_path(r'^(?P<id>[0-9]+)/twitter/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<sentiment>[a-zA-Z]+)/(?P<type>[a-zA-Z]+)/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<skip>[0-9]+)/$', v1.text1),
    re_path(r'^(?P<id>[0-9]+)/twitter/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<sentiment>[a-zA-Z]+)/(?P<type>[a-zA-Z]+)/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.text),
	re_path(r'^(?P<id>[0-9]+)/word/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<sentiment>[a-zA-Z]+)/(?P<type>[a-zA-Z]+)/(?P<top>[A-Za-z]+)/(?P<word>.*)/(?P<assoc>.*)/$', v1.wordassoc),
    re_path(r'^(?P<id>[0-9]+)/word/(?P<min>\d{0,15})/(?P<max>\d{10,18})/(?P<sentiment>[a-zA-Z]+)/(?P<type>[a-zA-Z]+)/(?P<top>[A-Za-z]+)/(?P<word>.*)/$', v1.wordcloud),
    path('forgot/', views.forgot),
    path('login', views.login),
    path('logout', views.logout),
    path('', v.home),
    path('register', views.register),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    views.activate, name='activate'),
    re_path(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    views.reset, name='reset'),
]
