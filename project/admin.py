# Contains settings for the admin panel.  Contains settings for project model i.e the page that will appear at 34.66.198.6/admin/projects
from django.contrib import admin
import datetime
from .models import project,tokens
from .models import tokens,hourlycount,lastcount
from django.shortcuts import redirect
from django.utils.html import format_html
from .views import deletee,stopping, start_new_project
from . import views as v
import threading
from threading import enumerate,Event
from django.urls import reverse
from .models import tweets
from django.urls import re_path
import ctypes
import time
from visualize.documents import TweetsDocument

# Register your models here.
class projectAdmin(admin.ModelAdmin):
# The options to be displayed when login of admin panel is opened. actions_html, actions_html1 and actions_html2 are buttons used for deleting project,
# deleting project data and stopping/starting project
 list_display=['name','id','user','end_date','lang','start_date','running','Count','actions_html','actions_html1','actions_html2']
 # Fields to search for in the search bar placed in the projects admin panel
 search_fields = ['name', 'id', 'user','lang','start_date','end_date']
# Function that maps url to admin functions. Works in the same way as urls.py placed in Twitter/ directory. The urls defined here will be used by reverse
 # below to get the required url for each button. Each url is assigned a name and on calling the name the required url will be fetched. 
 def get_urls(self):
        urls = super().get_urls()
# Id is a unique parameter for each project. It is a self incrementing value and will be used to identify which user is project 
# referenced in the call to delete project, delete data or start/stop
        custom_urls = [
            re_path(
                r'^deletep/(?P<id>[0-9]+)$',
                 self.admin_site.admin_view(self.deletep),
                name='deletep',
            ),
			re_path(
                r'^deletedata/(?P<id>[0-9]+)$',
                 self.admin_site.admin_view(self.deletedata),
                name='deletedata',
            ),
			re_path(
                r'^start/(?P<id>[0-9]+)$',
                 self.admin_site.admin_view(self.start),
                name='start',
            ),
			re_path(
                r'^stop/(?P<id>[0-9]+)$',
                 self.admin_site.admin_view(self.stop),
                name='stop',
            )
        ]
        return custom_urls + urls
# Function to render each delete project button in a row. This function acts as HTML and CSS for each button. The reverse function will generate the onclick or href parameter
# for each button. The admin::deletep is the function that will be called when button is clicked and arguments passed to the function will be the id of the
# project at each row. The id is required to generate the correct url i.e deletep/1 where 1 is the id of the project that is to be deleted . Each id is a unique parameter
# for the project
 def actions_html(self, obj):
    return format_html('<a class="button"  href={}  type="button">Delete</button>',reverse('admin:deletep', args=[obj.id]))
# Function to render each delete data button in a row. This function acts as HTML and CSS for each button. The reverse function will generate the onclick or href parameter
# for each button. The admin::deletedata is the function that will be called when button is clicked and arguments passed to the function will be the id of the
# project at each row. The id is required to generate the correct url i.e deletedata/1 where 1 is the id of the project whose data is to be deleted . Each id is a unique parameter
# for the project	
 def actions_html1(self, obj):
    return format_html('<a class="button"  href={}  type="button">Delete</button>',reverse('admin:deletedata', args=[obj.id]))
# Function to render each start/stop button in a row. This function acts as HTML and CSS for each button. The reverse function will generate the onclick or href parameter
# for each button. The admin::stop/admin::start is the function that will be called when button is clicked and arguments passed to the function will be the id of the
# project at each row. If the project is already started then admin::stop is called otherwise admin::start is called.The id is required to generate the 
# correct url i.e start/1 where 1 is the id of the project that is to be started . Each id is a unique parameter for the project. If project is already started
# then display stop on the button and admin::stop will be called otherwise start willbe displayed and admin::Start will be called
 def actions_html2(self, obj):
    nameproject="project"+str(obj.id)
    if nameproject in v.list and v.list[nameproject].is_alive() is True:
        return format_html('<a class="button"  href={}  type="button">Stop</button>',reverse('admin:stop', args=[obj.id])) 
    else:
        return format_html('<a class="button"  href={}  type="button">Start</button>',reverse('admin:start', args=[obj.id])) 
# Function to display the counts of the tweets. For this pymongo is used to calculate all the tweets that are collected under the project
# id
 def Count(self,obj):
    results=TweetsDocument.search().filter("term",Project_id=obj.id)
    return results.count()

# Allowing html Tags
 actions_html.allow_tags = True
 actions_html1.allow_tags = True
 actions_html2.allow_tags = True
# Text to show on top bar of the Table that is listing all the projects. Acts as <th> parameter for the new buttons defined whereas the buttons listed act as
# <td> parameters
 actions_html.short_description = "Delete Project"
 actions_html1.short_description = "Delete Data"
 actions_html2.short_description = "Start Stop"

# Function that will be used to delete the project
 def deletep(self, request, id, *args, **kwargs):
# Deleting a project requires deleting all the tweets collected under the project id and also stopping the project if it is started.
      delete_project=project.objects.get(id=id)
      stopping(id)
      deletee(id)
      delete_project.delete()
      return redirect('../../project/')
# Function that is used to delete all the project data collected under the project id
 def deletedata(self, request, id, *args, **kwargs):
      deletee(id)
      return redirect('../../project/')
# Function to start the project. This will create a new thread with name project<id> where id will be the id of the proeject.
# This name will be used to stop the project when required. For stopping, the corresponding event that will be created in the start project
# is set. On stream function, the condition for event set in checked on each on_data. If it is set then stream will be disconnected and the 
# thread will be closed.
 def start(self, request, id, *args, **kwargs):
# As language is boolean variable so converting it into string of required language. en and ar are used by tweepy for English and Arabic respectively
      project_start=project.objects.get(id=int(id))
      project_start.running=True
      project_start.save()
      start_new_project(project_start.keywords,project_start.lang,project_start.id,project_start.location,project_start.start_date) 
      return redirect('../../project/')
# Function to stop the project. It sets the corresponding event of the project and as a result, the stream will be disconnected
 def stop(self, request, id, *args, **kwargs):
    project_stop=project.objects.filter(id=id).update(running=False)
    stopping(id)
    return redirect('../../project/')

class tokensAdmin(admin.ModelAdmin):
 list_display=['access_token','running','last_error','Last_24H_Count']
 def Last_24H_Count(self,obj):
    now = datetime.datetime.now(datetime.timezone.utc)
    counts=hourlycount.objects.filter(access_token=obj.access_token,Time__gte=(now-datetime.timedelta(hours=24)))
    c=0
    for count in counts:
       c=c+count.count
    counts=lastcount.objects.get(access_token=obj.access_token,Time__gte=(now-datetime.timedelta(hours=24)))
    c=counts.count+c
    return c
class hourlycountAdmin(admin.ModelAdmin):
 list_display=['_id','access_token','HourlyTime','count']
 def HourlyTime(self,obj):
    Time=(obj.Time)+datetime.timedelta(hours=3)
    return Time

 # Registering the models with the modelAdmins so that the models can be shown in the admin page
class lastcountAdmin(admin.ModelAdmin):
 list_display=['_id','access_token','Last_Time','count']
 def Last_Time(self,obj):
    Time=(obj.Time)+datetime.timedelta(hours=3)
    return Time

admin.site.register(tokens,tokensAdmin)
admin.site.register(hourlycount,hourlycountAdmin)
admin.site.register(lastcount,lastcountAdmin)
# Registering the models with the modelAdmins so that the models can be shown in the admin page
admin.site.register(project,projectAdmin)

