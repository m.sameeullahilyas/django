# This file contains functions that are used for starting, stopping, deleting and other functions that are related to projects
from django.shortcuts import render,redirect,HttpResponse
import os
from login.models import user
from signal import signal, SIGTERM
from threading import Thread
from .models import project
from multiprocessing import Process
from django.utils.encoding import force_bytes, force_text
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from login.tokens import account_activation_token
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from .Stream import start_project
import re
from datetime import datetime,timedelta
import time
from visualize.documents import TweetsDocument
from django.core.serializers.json import DjangoJSONEncoder
import json
import csv
from .search_project import search_project
import sys

# A global list that will contain the corresponding events of the projects to see if they are atopped by the user or not.
list= {}
progress_json={}
download_json={}
download_csv={}
progress_csv={}
# Create your views here.
# Function for generating the home page
def home(request):
# Check if user is logged in or not
    if ('user' not in request.session) or ('pass' not in request.session):
        return redirect('/login')

    try:
        user_id=user.objects.get(email=request.session['user'],password=request.session['pass'])
        id=user_id.email

    except user.DoesNotExist:
        request.session['error_msg']= "Not Logged In"
        return redirect('/login')

    Count = project.objects.filter(user=id).count()
    Projects=project.objects.filter(user=id)
      
    if 'success_msg' in request.session:
        
        context={
			"project":Projects,
       	"count":Count,
		"success_msg":request.session['success_msg']
		}
        request.session['success_msg']=None
# Counting the number of tweets under each project and also whether the project is running or not. For running, the enumerate function is used
# which shows the active threads. The name of the threads are being checked. Each thread of project that is created has a unique name equal to 
# project<id>. If the name is listed in active threads then project is running and collecting tweets otherwise not
    tweet_count=[]

    for p in Projects:
        results = TweetsDocument.search().filter("term",Project_id=p.id)
        results=results.count()
        tweet_count.append(results)

    
    context["project"]=zip(Projects,tweet_count)
   
    if "error_msg" in request.session:
        context["error_msg"]=request.session['error_msg']
        request.session['error_msg']=None
    
    if "default"  in request.session:
         context["default"]=request.session["default"]
         request.session["default"]=None
    return render(request,'index.html',context)
# Function to create a new project
def create_project(request):
# Check if user is logged in or not
    if ('user' not in request.session) or ('pass' not in request.session):
        return redirect('/login')
    
    try:
        user_id=user.objects.get(email=request.session['user'],password=request.session['pass'])
        id=user_id.email

    except user.DoesNotExist:
        request.session['error_msg']= "Not Logged In"
        return redirect('/login')
# If method is post then save the required parameters. Also start the project on creation.
    if request.method =="POST": 
            if request.POST.get('Date'):
                project_create=project(
		  	user=id,
			name=request.POST.get('Name'),
			end_date=datetime.strptime(request.POST.get('Date'),"%m/%d/%Y"),
		        start_date=datetime.strptime(request.POST.get('Start-Date'),"%m/%d/%Y"),
			lang=request.POST.get('language'),
			keywords=request.POST.get('keywords'),
			location=request.POST.get('location'),
		        running=False,
                        search=False
		)
            else:
                project_create=project(
	                user=id,
                        end_date=None,
		        start_date=datetime.strptime(request.POST.get('Start-Date'),"%m/%d/%Y"),
			name=request.POST.get('Name'),
			lang=request.POST.get('language'),
			keywords=request.POST.get('keywords'),
			location=request.POST.get('location'),
		        running=False,
                        search=False
		)
            try:
                project_create.save()
                if project_create.start_date.date()<=(datetime.now()+timedelta(hours=3)).date():
                    project_create.running=True
                    project_create.save()
                    start_new_project(project_create.keywords,project_create.lang,project_create.id,project_create.location,project_create.start_date.date())  
                if "another" in request.POST:
                    request.session["default"]="c"
                if "continue" in request.POST:
                    return redirect('edit/'+str(project_create.id))


            except Exception as e:
                print(str(e))
                request.session["error_msg"]=e
                request.session["default"]="c"

    return redirect('/')

# Function to edit the account settings of the user. If new email is entered then user will have to reverify the email. A new token is sent to the user for
# verification
def account_edit(request):
    user_edit=user.objects.get(email=request.session['user'])
    if request.method=="POST":
        user_edit.first_name=request.POST.get("First Name")
        user_edit.last_name=request.POST.get("Last Name")
        user_edit.phone=request.POST.get("Phone")
        if user_edit.password!=request.POST.get("Password"):
           password=(request.POST.get('Password'))
           user_edit.password=password
        if user_edit.email!=request.POST.get("email"):
           project.objects.filter(user=user_edit.email).update(user=request.POST.get("email"))
           user_edit.email=request.POST.get("email")
           user_edit.is_verified=False
          
        try:
           user_edit.save()
           if not user_edit.is_verified:
               mail_subject = 'Activate your account.'
               current_site = get_current_site(request)
               message = render_to_string('acc_active_email.html', {
                'First_Name': user_edit.first_name,
                'Last_Name': user_edit.last_name,
                'Phone#': user_edit.phone,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user_edit._id)),
                'token':account_activation_token.make_token(user_edit),
               })
			
               toemail = user_edit.email
               email = EmailMessage(
                        mail_subject, message, to=[toemail]
                )
               email.send()
               context={"success_msg":"Account details are changed. Since Email is changed so check your mail for verification otherwise you wont be able to login"}
				
           else:
               context={"success_msg":"Account details are successfully changed"}
        except Exception as e:
               context={"error_msg":e}
	
        if "success_msg" in context:
            request.session["success_msg"]=context["success_msg"]
            request.session["user"]=user_edit.email
            request.session["pass"]=user_edit.password
            return redirect('/')
        elif "error_msg" in context:
            context["user"]=user_edit
    else:
          context={"user":user_edit}
    return render(request,'account_edit.html',context)
# Function to start a new thread when a new project is being created. This function is a decorator. Each new thread that will be responsible for
# handling the stream of the project will have a unique name equal to project<id> where id is the unique id of the project. The thread will be a daemon
# thread. This unique name will be used for identifying whether a project is running or not and also for stopping the project
def start_new_thread(function):
    def decorator(*args, **kwargs):
        nameproject="project"+str(args[2])
        t = Process(target = function, args=args)
        list[nameproject]=t
        t.daemon = True
        from django.db import connections
        connections.close_all()
        t.start()
    return decorator

def start_new_search(function):
    def decorator(*args, **kwargs):
        t = Thread(target = function, args=args, kwargs=kwargs)
        t.start()
    return decorator

@start_new_search
def new_search(keywords,lang,id,follow,coordinates,token,start_date,db):
  search_project(keywords,lang,id,follow,coordinates,token,start_date,db)

# Starting the new thread with decorator mentioned. Supplies the start_project function with required parameters
@start_new_thread
def start_new_project(keyword,lang,id,coordinate,start_date):
 from elasticsearch_dsl.connections import connections
 from pymongo import MongoClient
 connections.create_connection(hosts=['localhost'])
 client = MongoClient('mongodb://localhost:27017/')
 db1 = client.project
 token_search=None
 token=db1.project_tokens.find_and_modify({"running":1},{"$inc":{"running":1}})
 if not token:
       token=db1.project_tokens.find_and_modify({"running":0},{"$inc":{"running":1}})
 if not token:
       project_stop=project.objects.filter(id=id).update(running=False)
       print('ended')
       return

 keywords=re.split(",|،", keyword)
 keywords=[k.strip() for k in keywords]
 if(coordinate):
      coordinates=coordinate.split(",")
      coordinates=[float(x) for x in coordinates]
 else:
      coordinates=None
 users=[x for x in keywords if re.match('@',x)]
 if(len(users)):
        follow=[]
        for x in users:
             a=x.find(':')
             if a!=-1:
                 follow.append(x[a+1:])
        if len(follow)==0:
           follow=None
 else :
      follow=None
 search=db1.project_project.find_one({"id":int(id)})
 if start_date<(datetime.now()+timedelta(hours=3)).date() and start_date>=(datetime.now()+timedelta(hours=3)-timedelta(days=7)).date() and not search['search']:
        token_search=db1.project_tokens.find_and_modify({"running":1},{"$inc":{"running":1}})
        if not token_search:
             token_search=db1.project_tokens.find_and_modify({"running":0},{"$inc":{"running":1}})
        if not token_search:
             project_stop=project.objects.filter(id=id).update(running=False)
             db1.project_tokens.update_one({"access_token":token['access_token']},{"$inc":{"running":-1}})
             print('ended')
             return
        print('Starting Historical Search')
        new_search(keywords,lang,id,follow,coordinates,token_search,start_date,db1)

 def end(*args):
      if token_search:
          db1.project_tokens.update_one({"access_token":token_search['access_token']},{"$inc":{"running":-1}})

      db1.project_tokens.update_one({"access_token":token['access_token']},{"$inc":{"running":-1}})
      print('ended')
      client.close()
      sys.exit(0)

 signal(SIGTERM,end)
 while 1:
      start_project(keywords,lang,id,follow,coordinates,token,db1)

def deletefile(id,type):
 if type=='json' and id in progress_json:
         time.sleep(60)
         del progress_json[id]
 if type=='csv' and id in progress_csv:
         time.sleep(60)
         del progress_csv[id]

# Function to download the csv of the project. Have to write the csv and give it to response 
def downloadcsv(request,id,name):
 if id in download_csv:
        response=HttpResponse(content_type='application/force-download')
        response['Content-Disposition'] = 'attachment; filename=%s'%smart_str("Project"+id+'.csv')
        response['Content-Length']= str(os.path.getsize(download_csv[id].name))
        response['X-Sendfile'] = smart_str(download_csv[id].name)
        t=Thread(target=deletefile,args=[id,'csv'])
        t.daemon=True
        t.start()
        return response



def downloadcsv_thread(id):
 s=TweetsDocument.search().filter("term",Project_id=id)
 count=s.count()
 i=0
 from tempfile import NamedTemporaryFile
 f = NamedTemporaryFile(dir='/home/acumen_analytica_dev1/files/',mode='w+',suffix='.csv')
 writer = csv.writer(f)
 writer.writerow(['Created At', 'Hashtags', 'Mentions','Quoted Screen Name','Is Quote','Is Retweet','Coordinates','Cleaned Text','Status Id','Sentiment','Screen Name','Timestamp','Type','Retweet Count','Retweet Screen Name','Text'])
 data=[]
 for doc in s.scan():
         data.append([doc.created_at,doc.hashtags,doc.mentions_screen_name,doc.quoted_screen_name,doc.is_quote,doc.is_retweet,doc.coordinates,doc.cleaned_text,doc.status_id,doc.sentiment_analysis,doc.screen_name,doc.timestamp_ms,doc.type,doc.retweet_count,doc.retweet_screen_name,doc.text])
         i+=1
         if i%100000==0 or i==count:
             writer.writerows(data)
         progress_csv[id]=(i/count)*100
 download_csv[id]=f
 return

from django.utils.encoding import smart_str

# Function to download the project data as json. For newline json ndjson is used.
def downloadjson(request,id,name):
 if id in download_json:
        response=HttpResponse(content_type='application/force-download')
        response['Content-Disposition'] = 'attachment; filename=%s'%smart_str("Project"+id+'.json')
        response['Content-Length']= str(os.path.getsize(download_json[id].name))
        response['X-Sendfile'] = smart_str(download_json[id].name)
        t=Thread(target=deletefile,args=[id,'json'])
        t.daemon=True
        t.start()
        return response
def downloadjson_thread(id):
 from tempfile import NamedTemporaryFile
 f = NamedTemporaryFile(dir='/home/acumen_analytica_dev1/files/',mode='w+',suffix='.json')
 s=TweetsDocument.search().filter("term",Project_id=id)
 count=s.count()
 i=0
 data=[]
 for hit in s.scan():
   data.append([hit.to_dict()])
   i+=1
   if i%100000==0 or i ==count:
        f.write(json.dumps(data,cls=DjangoJSONEncoder,ensure_ascii=False, indent=4)	)
        f.flush()
        data=[]
   progress_json[id]=((i/count)*100)
 download_json[id]=f

def prog(request,id,type):
 if type=='json':
     if id in progress_json:
         return HttpResponse(json.dumps([progress_json[id]]), content_type='application/json') 
     progress_json[id]=0
     t = Thread(target = downloadjson_thread, args=[id])
     t.start()
     return HttpResponse(json.dumps([0]), content_type='application/json')

 if type=='csv':
     if id in progress_csv:
         return HttpResponse(json.dumps([progress_csv[id]]), content_type='application/json')

     progress_csv[id]=0
     t = Thread(target = downloadcsv_thread, args=[id])
     t.start()

     return HttpResponse(json.dumps([0]), content_type='application/json')



# Function to start the project. This will create a new event and save it to list with key being project<id> i.e. the event is being saved with a key that is
# equal to the name of the thread that will be created. This will be used to stop the project. Having same key as name of thread, we can identify which 
# event is of which project and can set the event which will result in stopping of the project.
def start(request,id):
 try:
    project_start=project.objects.get(id=int(id))
    project_start.running=True
    project_start.save()
    start_new_project(project_start.keywords,project_start.lang,project_start.id,project_start.location,project_start.start_date)
    return redirect('/')
 except project.DoesNotExist:
    request.session["error_msg"]="No such project exists"
    return redirect('/') 
# Function to delete the project data

def delete_thread(function):
    def decorator(*args, **kwargs):
        t = Process(target = function, args=args)
        t.daemon = True
        from django.db import connections
        connections.close_all()
        t.start()
    return decorator


@delete_thread
def deletee(id):
  from pymongo import MongoClient
  from project.models import tweets
  client = MongoClient('mongodb://localhost:27017/')
  db=client.project
  s=TweetsDocument.search().filter("term",Project_id=int(id))
  if s.count()>0:
      try:
         s.delete()
      except:
         tweets.objects.filter(Project_id=int(id)).delete()
  db.project_tweets.remove({"Project_id":int(id)})
  client.close()
def delete(request,id):
       deletee(id)
       return redirect('/')
# Function to edit the project settings
def edit(request,id):
   try:
       project_start=project.objects.get(id=int(id))
       if(project_start.end_date):
           t = project_start.end_date.strftime("%m/%d/%Y")
           project_start.end_date=t

       t = project_start.start_date.strftime("%m/%d/%Y")
       project_start.start_date=t

       if request.method=="POST":
            project_start.name=request.POST.get('Name')
            if(request.POST.get('Date') and request.POST.get('Date')!='None' and request.POST.get('Date')!=''):
                    try:
                        project_start.end_date=datetime.strptime(request.POST.get('Date'),"%m/%d/%Y")
                    except:
                        request.session["error_msg"]="End Date is not Correct"
                        return redirect('/')
            else:
                project_start.end_date=None
            project_start.lang=request.POST.get('language')
            try:
                project_start.start_date=datetime.strptime(request.POST.get('Start-Date'),"%m/%d/%Y")
            except:
                    request.session["error_msg"]="Start Date is not Correct"
                    return redirect('/')

            project_start.keywords=request.POST.get('keywords')
            project_start.location=request.POST.get('location')
            try:
                project_start.save()
                request.session["success_msg"]="Project is edited"
                return redirect('/')
            except Exception as e:
                request.session["error_msg"]=e
   except project.DoesNotExist:
        request.session["error_msg"]="No such project exists"
        return redirect('/')

   return render(request,'edit.html',{"project":project_start})
# Function to delete the project. For deleting project, delete is also called so that all data of project is also deleted
def delete_project(request,id):
   try:
       delete_project=project.objects.get(id=id)
       stopping(id)
       deletee(id)
       delete_project.delete()
       return redirect('/')
   except:
       request.session["error_msg"]="No such project exists"
       return redirect('/')
# For stopping the project, event is set which sends a signal to the stream to stop
def stop_project(request,id):
    project_stop=project.objects.filter(id=id).update(running=False)
    stopping(id)
    return redirect('/') 

def job():
    from .models import project
    project_start=project.objects.filter(start_date=(datetime.now()+timedelta(hours=3)).date(),running=False)
    for projct in project_start:
             projct.running=True
             projct.save()
             start_new_project(projct.keywords,projct.lang,projct.id,projct.location,projct.start_date)

    project_end=project.objects.filter(end_date=(datetime.now()+timedelta(hours=3)).date(),running=True)
    for projct in project_end:
             projct.running=False
             projct.save()
             stopping(projct.id)

def stopping(id):
    name="project"+id
    if name in list:
           list[name].terminate()
           time.sleep(1)

def resume():
    running_projects=project.objects.filter(running=True)
    for running_project in running_projects:
        start_new_project(running_project.keywords,running_project.lang,running_project.id,running_project.location,running_project.start_date)

def o():
  from pymongo import MongoClient
  client=MongoClient("localhost:27017")
  db=client.project
  coll=db.project_tweets
  i=0
  t=coll.find({"Project_id":11})
  from project.models import tweets
  print("start")
  for json_data1 in t:
      i+=1
      if i%1000==0:
          print(i)
      s=TweetsDocument.search().filter("term",status_id=json_data1["status_id"]).filter("term",Project_id=11).count()
      if s==0:
                    t =tweets.objects.get(id=json_data1["id"])
                    t.save()
