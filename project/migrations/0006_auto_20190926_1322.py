# Generated by Django 2.2.2 on 2019-09-26 13:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0005_tweets_id_str'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='end_date',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='location',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
