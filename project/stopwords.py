from nltk.corpus import stopwords
import codecs
import os
    
		# prepare the stop words list
def getStopWords():
        arStopwords = stopwords.words('arabic')
        enStopWords = stopwords.words('english')
        newStopWords = []
        __location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
        INPUT_DIR = os.path.join(__location__, 'arabic_stopwords.txt')
        file = codecs.open(INPUT_DIR, "r", encoding='utf-8')
        for line in file:
            line = " ".join(line.split())
            newStopWords.append(line)
        arStopwords.extend(enStopWords)
        arStopwords.extend(newStopWords)
        return set(arStopwords)

def getPersianStopWords():
        enStopWords = stopwords.words('english')
        newStopWords = []
        __location__ = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__)))
        INPUT_DIR = os.path.join(__location__, 'short.txt')
        file = codecs.open(INPUT_DIR, "r", encoding='utf-8')
        for line in file:
            line = " ".join(line.split())
            newStopWords.append(line)
        newStopWords.extend(enStopWords)
        return set(newStopWords)