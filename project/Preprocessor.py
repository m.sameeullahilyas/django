
# coding: utf-8

# In[6]:

import html.parser
import string
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import re
import pyarabic.araby as araby
from sacremoses import MosesDetokenizer
from nltk.tokenize import word_tokenize
from nltk.stem.isri import ISRIStemmer
from .stopwords import getStopWords,getPersianStopWords


def preprocesser(text):
    # convert to lower
    content = text.lower()
    # content = ' '.join(word for word in content2.split(' ') if not  word.startswith('@'))
    # remove @usernames
    content = re.sub(r'@([a-z0-9_]+)', ' ', content)
    # remove URLs
    content = re.sub(r"http\S+", "", content)
    # remove underscore
    content = content.replace("_", " ")
    # remove Tashkeel
    content = araby.strip_tashkeel(content)
    # remove !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
    content = re.sub(r'[^\w\s]', ' ', content)
    # content = ' '.join(word.strip(string.punctuation) for word in content.split())
    # replace Spaces, Tabs, Newlines with space
    content = " ".join(content.split())
    tokens = word_tokenize(content)
    tokens = [item for item in tokens if not item.isdigit() and not re.match(r'[A-Z]+', item, re.I)]
    detokenizer = MosesDetokenizer()
    cleaned_text = detokenizer.detokenize(tokens, return_str=True)
    return cleaned_text

#---  Pre-processor:  
#(1) preprocessor function -- Return detokenized data
def preprocessor(text):
    #convert to lower
    content = text.lower()
    #content = ' '.join(word for word in content2.split(' ') if not  word.startswith('@'))
    #remove @usernames
    content = re.sub(r'@([a-z0-9_]+)',' ',content,flags=re.U)
    #remove URLs
    content = re.sub(r"http\S+", "", content,flags=re.U)
    #remove underscore
    #content = content.replace("_", " ")
    #Remove Tatweel
    content= araby.strip_tatweel(content)
    #remove Tashkeel
    content=  araby.strip_tashkeel(content)
    #normalize ligature -- Normalize Lam Alef ligatures into two letters (LAM and ALEF),
    #and Tand return a result text.
    content = araby.normalize_ligature(content)
    #remove repeated characters
    content = re.sub(r'(.)\1+', r'\1', content)
    #normalization
    content = re.sub("[إأآا]", "ا", content)
    content = re.sub("ى", "ي", content)
    content = re.sub("ة", "ه", content)
    content = re.sub("گ", "ك", content)
    content = re.sub("ؤ", "و", content) # ؟؟؟
    #remove !"#$%&'()*+,-./:;<=>?@[\]^`{|}~
    content = re.sub(r'[^\w\s]',' ',content,flags=re.U)
    #content = ' '.join(word.strip(string.punctuation) for word in content.split())
    #replace Spaces, Tabs, Newlines with space
    content = " ".join(content.split())
    #Tokenize the text
    tokens = word_tokenize(content)
    #Remove digits and Remove English words
    tokens = [item for item in tokens if not item.isdigit() and not re.match(r'[A-Z]+', item, re.I)]
    #Light Stemmer
    stemmer = ISRIStemmer()
    stemmed1 = [stemmer.norm(word, num=1) for word in tokens]  # remove diacritics which representing Arabic short vowels
    #stemmed2 = [stemmer.pre32(word) for word in stemmed1]  # remove length three and length two prefixes in this order
    #stemmed3 = [stemmer.waw(word)   for word in stemmed1] # remove connective ‘و’ if it precedes a word beginning with ‘و’
    stemmed4 = [stemmer.norm(word, num=2) for word in stemmed1]  # normalize initial hamza to bare alif
    detokenizer = MosesDetokenizer()
    cleaned_text = detokenizer.detokenize(stemmed4, return_str=True)
    return cleaned_text
	
def engtext(lang,text):
 if lang=="en":
     text = text.lower() 
 html_parser = html.parser.HTMLParser()
 text = re.sub(r"http\S+", "", text)
 text = re.sub(r"www\S+", "", text)
 text = re.sub(r"https\S+", "", text)
 text = re.sub(r'@([a-z0-9_\S+]+)', '', text)
 text = re.sub(r'[^\w\s]',' ',text,flags=re.U)
 text = html.unescape(text)
 text = " ".join(text.split())
 tokens = word_tokenize(text) 
 if lang=="fa":
         stop_words = getPersianStopWords()
 else:
         stop_words= getStopWords() 
 words = [w for w in tokens if not w in stop_words] 
 if lang=="en":
     tokens = [item for item in words if not item.isdigit() and re.match(r'[A-Z]+', item, re.I) and len(item)>1 and not item.isnumeric()]
 else:
     tokens = [item for item in words if not item.isdigit() and not re.match(r'[A-Z]+', item, re.I) and len(item)>1 and not item.isnumeric()]  
 text = " ".join(tokens)
 return text
