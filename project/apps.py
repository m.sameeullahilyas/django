from django.apps import AppConfig


class ProjectConfig(AppConfig):
    name = 'project'
    def ready(self):
        from .views import job,resume
        from datetime import datetime,timedelta
        from apscheduler.schedulers.background import BackgroundScheduler
        scheduler = BackgroundScheduler()
        mylist=[21]
        t=datetime.now()
        h=min(mylist, key=lambda x:abs(x-int(t.hour)))
        start=str(t.date())+ " "+str(h)+":00:00"
        scheduler.add_job(resume,'date',run_date=datetime.now()+timedelta(minutes=1))
        scheduler.add_job(job,'interval',hours=24,start_date=start,end_date='3019-08-2 1:17:11')
        scheduler.start()
        for job in scheduler.get_jobs():
            print("name: %s, trigger: %s, next run: %s, handler: %s" % (
                job.name, job.trigger, job.next_run_time, job.func))
            #job.modify(next_run_time=datetime.now()+timedelta(seconds=0))
        print("Scheduler started!")


