# This file defines the project and tokens model. They contains fields that will be stored in the Db
from django.db import models
from datetime import date
from djongo import models as s
from django.core.exceptions import ValidationError
from datetime import datetime,timedelta
# Each project model contains a unique id which is a self increamenting value field. 
# Create your models here.
class project(models.Model):
    id=models.AutoField(primary_key=True)
    user = models.EmailField(db_index=True)
    name=models.CharField(max_length=200)  
    end_date=models.DateField(null=True,blank=True)
    lang=models.CharField(max_length=2)	
    keywords=models.TextField()
    start_date=models.DateField(default=date.today)
    location=models.CharField(max_length=200,null=True,blank=True)
    running=models.BooleanField()
    search=models.BooleanField(default=False)
    def clean(self, *args, **kwargs):
        # run the base validation
        super(project, self).clean(*args, **kwargs)
        # Don't allow dates older than now.
        if self.end_date!=None and self.end_date!='':
            if self.start_date >=self.end_date:
                raise ValidationError('Start Date must be less than End Date.')
        if self.start_date<(datetime.today()-timedelta(days=7)).date():
             raise ValidationError('Start Date must be greated than '+str((datetime.today()-timedelta(days=7)).date()))

    def save(self, *args, **kwargs):
         if not self.end_date or self.end_date=='':
              self.end_date = None
         if not self.location and self.location!='':
              self.location = ''
         super(project, self).save(*args, **kwargs)

    class Meta:
       indexes = [
           models.Index(fields=['user'])
		   ]
		   
# Each token has a running field which indicates how many projects are using this token.
class tokens(models.Model):
    consumer_key=models.CharField(max_length=200)
    consumer_secret=models.CharField(max_length=200)
    access_token=models.CharField(max_length=200)
    access_key=models.CharField(max_length=200)
    running=models.IntegerField()
    last_error=models.CharField(max_length=1024) 

class hourlycount(models.Model):
	_id = s.ObjectIdField(primary_key=True)
	count=models.IntegerField()
	Time=models.DateTimeField()
	access_token=models.CharField(max_length=200)

class lastcount(models.Model):
	_id = s.ObjectIdField(primary_key=True)
	count=models.IntegerField()
	Time=models.DateTimeField()
	access_token=models.CharField(max_length=200)

class tweets(models.Model): #Collection Name
    Project_id=models.IntegerField()
    text =  models.TextField(null=True)
    cleaned_text = s.ListField(default=[])
    hashtags =  s.ListField(default=[])
    mentions_screen_name =s.ListField(default=[])
    screen_name =  models.TextField(null=True)
    sentiment_analysis = models.TextField(null=True)
    type = models.TextField(null=True)
    retweet_screen_name = models.TextField(null=True)
    status_id = models.TextField(null=True)
    created_at=models.DateTimeField()
    probability=models.FloatField()
    timestamp_ms=models.BigIntegerField()
    quoted_screen_name=models.TextField(null=True)
    coordinates=s.ListField()
    is_retweet=models.BooleanField()
    is_quote=models.BooleanField()
    retweet_count=models.IntegerField()
    profile_image_url_https=models.TextField(null=True)
    id_str=models.TextField(null=True)

