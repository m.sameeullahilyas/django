# This file is used to calculate the sentiment anlysis of the tweets
import codecs
import re
import os
from .Preprocessor import preprocessor,preprocesser,engtext
from .stopwords import getStopWords,getPersianStopWords
from nltk.tokenize import word_tokenize
import csv
from flashtext.keyword import KeywordProcessor
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
# This class will be used to calculate the sentiment of arabic tweets

class arabic_sentiment_score():
    list_of_stopWords = getStopWords()

    stopWords = [preprocesser(stop) for stop in list_of_stopWords]

    stopWords = list(filter(None, stopWords))
    stopWords_re = re.compile(r'\b(?:%s)\b' % '|'.join(stopWords))

    posTokens_processor = KeywordProcessor()
    negTokens_processor = KeywordProcessor()

	# SMAT lexicon csv
    __location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(__location__, "Manually_filtered_lexicon2.csv"),
	          "r", encoding="utf-8") as read_file:
	    csv_reader = csv.reader(read_file, delimiter=',')
	    for row in csv_reader:
		    if row[1] == "Pos":
				# posTokens.append(row[0])
			    posTokens_processor.add_keyword(row[0])
		    else:
				# negTokens.append(row[0])
			    negTokens_processor.add_keyword(row[0])

    read_file.close()
	# add the negation words to a list
    negation_list = []
    with open(os.path.join(__location__, "negation_list.csv"), "r",
	          encoding="utf-8") as read_file:
	    csv_reader = csv.reader(read_file, delimiter=',')
	    for row in csv_reader:
		    negation_list.append(row[0])
    read_file.close()

	# add the pos emojis to the existing pos lexicon lists
    with open(os.path.join(__location__, "pos_emojis.csv"), "r",
	          encoding="utf-8") as read_file:
	    csv_reader = csv.reader(read_file, delimiter=',')
	    for row in csv_reader:
			# posTokens.append(row[0])
		    posTokens_processor.add_keyword(row[0])

    read_file.close()

	# add the neg emojis to the existing neg lexicon lists
    with open(os.path.join(__location__, "neg_emojis.csv"), "r",
	          encoding="utf-8") as read_file:
	    csv_reader = csv.reader(read_file, delimiter=',')
	    for row in csv_reader:
		    negTokens_processor.add_keyword(row[0])

    read_file.close()


    negation_list = list(filter(None, negation_list))
    negation_list_re = re.compile(r'\b(?:%s)\b' % '|'.join(negation_list))

	# calculate number of pos and neg words in a tweet and report tweet's score (use it with all lexicons)
    def calculator(self,tweet):
        tweet= preprocessor(tweet)
        negWords_found = []
        posWords_found = []


		# tokenize the tweet
        tweetTokens = word_tokenize(tweet)

		# Count the number of positive words
        t_len = len(tweetTokens)
        posWords_found.extend(self.posTokens_processor.extract_keywords(tweet))
		
		# Count the number of negative words
        negWords_found.extend(self.negTokens_processor.extract_keywords(tweet))

		# negation checking
        for token in tweetTokens:
            if self.negation_list_re.search(token):
                index = tweetTokens.index(token)
                nxt_ind = index + 1
                if nxt_ind < t_len:
                    n_word = tweetTokens[index + 1]
                    if n_word in posWords_found:
                        posWords_found.remove(n_word)
                        negWords_found.append(n_word)
                    elif n_word in negWords_found:
                        posWords_found.append(n_word)
                        negWords_found.remove(n_word)
# Calculate the total number of positive and negative words found in the tweet. If number>0 then positive is the sentiment. On 0 neutral and on number <0
# negative is the sentiment
        numNegWords = len(negWords_found)
        numPosWords = len(posWords_found)
        sum = (numPosWords - numNegWords)
        if sum >0:
           return "Positive"
        elif sum<0: 
             return  "Negative"
        else:
           return "Neutral"
		
# This class will be used to calculate the sentiment of persian tweets
class persian_sentiment_score():
    list_of_stopWords = getPersianStopWords()
    stopWords = [preprocesser(stop) for stop in list_of_stopWords]
    stopWords = list(filter(None, stopWords))
    stopWords_re = re.compile(r'\b(?:%s)\b' % '|'.join(stopWords))
    posTokens_processor = KeywordProcessor()
    negTokens_processor = KeywordProcessor()
    __location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
    INPUT_DIR = os.path.join(__location__, 'NRC-emotion-lexicon-wordlevel-persian-v0.92.txt')
    file = codecs.open(INPUT_DIR, "r",encoding='utf-8')
    for line in file:
        line = line.split()
        word = [item for item in line if not item.isdigit() and not re.match(r'[A-Z]+', item, re.I)]
        word = " ".join(word)

        if "positive" in line and line[len(line)-1]=='1':
             posTokens_processor.add_keyword(word)
        elif "negative" in line and line[len(line)-1]=='1':
             negTokens_processor.add_keyword(word)

    file.close()    
    # add the pos emojis to the existing pos lexicon lists
    with open(os.path.join(__location__, "pos_emojis.csv"), "r",
              encoding="utf-8") as read_file:
        csv_reader = csv.reader(read_file, delimiter=',')
        for row in csv_reader:
            # posTokens.append(row[0])
            posTokens_processor.add_keyword(row[0])

    read_file.close()

    # add the neg emojis to the existing neg lexicon lists
    with open(os.path.join(__location__, "neg_emojis.csv"), "r",
              encoding="utf-8") as read_file:
        csv_reader = csv.reader(read_file, delimiter=',')
        for row in csv_reader:
            negTokens_processor.add_keyword(row[0])

    read_file.close()

    # calculate number of pos and neg words in a tweet and report tweet's score (use it with all lexicons)
    def calculator(self,tweet):
        tweet= preprocessor(tweet)
        negWords_found = []
        posWords_found = []


        # tokenize the tweet
        tweetTokens = word_tokenize(tweet)

        # Count the number of positive words
        t_len = len(tweetTokens)
        posWords_found.extend(self.posTokens_processor.extract_keywords(tweet))
        
        # Count the number of negative words
        negWords_found.extend(self.negTokens_processor.extract_keywords(tweet))


# Calculate the total number of positive and negative words found in the tweet. If number>0 then positive is the sentiment. On 0 neutral and on number <0
# negative is the sentiment
        numNegWords = len(negWords_found)
        numPosWords = len(posWords_found)
        sum = (numPosWords - numNegWords)
        if sum >0:
           return "Positive"
        elif sum<0: 
             return  "Negative"
        else:
           return "Neutral"
		
# if language is english then use vaderSentiment otherwise use the code given. The compound scores are taken from the example of vaderSentiment given in 
# the documentation
def calculate_sentiment(lang,text):
    if(lang=='ar'):
         sentiment=arabic_sentiment_score()
         text=engtext(lang,text)
         sentiment=sentiment.calculator(text)
         return text,sentiment
    elif(lang=='fa'):
        sentiment=persian_sentiment_score()
        text=engtext(lang,text)
        sentiment=sentiment.calculator(text)
        return text,sentiment
    else:
        analyzer = SentimentIntensityAnalyzer()
        vs = analyzer.polarity_scores(text)
        text=engtext(lang,text)
        if(vs['compound']>=0.05):
            return text,"Positive"
        elif(vs['compound']<=-0.05):
            return text,"Negative"
        else:
            return text,"Neutral"
        
		
