import tweepy
import datetime
import math
from project.models import tweets
from visualize.documents import TweetsDocument
import json
import re
import time
import pytz
from .sentiment_analysis import calculate_sentiment
from dateutil import tz

# Starting the projects. First see if there is a token that is being used by only 1 project. If not then search for a new token 

def search_project(keywords,language,id,follow,coordinates,token,start_date,db):
   # Variables that contains the user credentials to access Twitter API
    ACCESS_TOKEN = token['access_token']
    ACCESS_TOKEN_SECRET = token['access_key']
    CONSUMER_KEY = token['consumer_key']
    CONSUMER_SECRET = token['consumer_secret']

#create an OAuthHandler instance and pass the consumer token and secret
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
# Create a wrapper for the API provided by Twitter
    api = tweepy.API(auth,wait_on_rate_limit=True,wait_on_rate_limit_notify=True)
    i=0
    while i<len(keywords):
        a=keywords[i]
        i+=1
        j=1
        while i<len(keywords):
            if len(a+' OR '+keywords[i])<500 and j<=10:
                a=a+' OR '+keywords[i]
                i+=1
                j+=1
            else:
                break
        if coordinates is not None:
                 lat=(coordinates[1]+coordinates[3])/2
                 lon=(coordinates[0]+coordinates[2])/2
                 d=getDistanceFromLatLonInKm(lat,lon,coordinates[3],coordinates[2])
                 geo=str(lat)+','+str(lon)+','+str(d)+'km'
                 for status in tweepy.Cursor(api.search,q=a,lang=language,geocode=geo,count=100,tweet_mode='extended').items():
                                    if (status.created_at+datetime.timedelta(hours=3)).date()>=start_date:
                                          json=status._json
                                          data(json,token['access_token'],db,id,[lon,lat])
                                    time.sleep(0.1)
        else:
             for status in tweepy.Cursor(api.search,q=a,lang=language,count=100,tweet_mode='extended').items():
                                   if (status.created_at+datetime.timedelta(hours=3)).date()>=start_date:
                                          json=status._json
                                          data(json,token['access_token'],db,id,[])
                                   time.sleep(0.1)
    db.project_project.update_one({"id":int(id)},{"$set":{"search":True}})
    db.project_tokens.update_one({"access_token":token['access_token']},{"$inc":{"running":-1}})
    print('Ended Historical Search')

def getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2):
  R = 6371
  dLat = math.radians(lat2-lat1)
  dLon = math.radians(lon2-lon1) 
  a = math.sin(dLat/2) * math.sin(dLat/2) + math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *  math.sin(dLon/2) * math.sin(dLon/2)
  c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a));
  d = R * c
  return d



def data(json_data,access,db,id,geo):
        try:
                    json_data1={}
                    prob=ComputeAccountProbability(json_data)
                    if json_data["retweeted"] is True:
                        if not json_data['retweeted_status']["is_quote_status"]:
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['retweeted_status']['full_text'])
                                 full_text=json_data['retweeted_status']['full_text']

                        else:
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['full_text'])
                                 full_text=json_data['full_text']

                    elif json_data['is_quote_status'] is True and 'quoted_status' in json_data: 
                        if not json_data['quoted_status']["retweeted"]:
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['quoted_status']['full_text'])                          
                                 full_text=json_data['quoted_status']['full_text']
                        else:
                                text,sentiment=calculate_sentiment(json_data['lang'],json_data['full_text'])                          
                                full_text=json_data['full_text']
                    else:
                            text,sentiment=calculate_sentiment(json_data['lang'],json_data['full_text'])
                            full_text=json_data['full_text']

# Save the parameters that are required for visualizing the data. Some of the data are used by the network analysis tool. Their name and structure
# are used in the way so that they can be used in the network analysis tool. Hashtags and mentions are list of strings. Type is entered so that tweets
# can be categorized in the tweet feed accordingly. 
                    json_data1['sentiment_analysis']=sentiment
                    json_data1['Project_id']=id
                    json_data1["text"]=full_text
                    json_data1["cleaned_text"]=text.split(" ")
                    json_data1['status_id']=json_data['id_str']
                    json_data1["hashtags"]=[]
                    json_data1["mentions_screen_name"]=[]
                    json_data1["retweet_screen_name"]=[]
                    json_data1["quoted_screen_name"]=[]
# Saving the hastags, mentions involved in the tweet. If retweet then hashtags and mentions will also be included in the retweet status, Same applies for
# Quoted tweet
                    if json_data["retweeted"] is True:
                        for hashtags in json_data['retweeted_status']['entities']['hashtags']:
                                 json_data1["hashtags"].append(hashtags["text"])
                        for mentions in json_data['retweeted_status']['entities']['user_mentions']:
                                     json_data1["mentions_screen_name"].append(mentions["screen_name"])

                    if json_data['is_quote_status'] is True and 'quoted_status' in json_data:
                        for hashtags in json_data['quoted_status']['entities']['hashtags']:
                                 json_data1["hashtags"].append(hashtags["text"])
                        for mentions in json_data['quoted_status']['entities']['user_mentions']:
                                 json_data1["mentions_screen_name"].append(mentions["screen_name"])                              
                    

                       
                    for hashtags in json_data['entities']['hashtags']:
                             json_data1["hashtags"].append(hashtags["text"])
                    for mentions in json_data['entities']['user_mentions']:
                             json_data1["mentions_screen_name"].append(mentions["screen_name"])

                    if "media" in json_data['entities']:
                        for media in json_data['entities']['media']:
                            if media['type']=='photo' or media['type']=='video':
                                 json_data1["type"]=media['type']
                            else:
                                 json_data1['type']="other"

                    elif len(json_data['entities']['urls']):
                        for urls in json_data['entities']['urls']:
                             json_data1["type"]="Links"                        
                    else:

                        json_data1['type']="other"                                          
                   
                    if json_data["retweeted"] is True:
                           json_data1["retweet_screen_name"]=json_data["retweeted_status"]["user"]["screen_name"]
                    if json_data["is_quote_status"] is True and "quoted_status" in json_data:
                           json_data1["quoted_screen_name"]=json_data["quoted_status"]["user"]["screen_name"]
                    
                    if(len(json_data1["retweet_screen_name"])==0):
                            json_data1["retweet_screen_name"]=""
                    
                    if(len(json_data1["quoted_screen_name"])==0):
                            json_data1["quoted_screen_name"]=""   
       
                    if(json_data["coordinates"]):
                        json_data1["coordinates"]=[]
                        json_data1["coordinates"].extend([json_data["coordinates"]["coordinates"][0],json_data["coordinates"]["coordinates"][1]])
                    elif(json_data["place"]):
                        json_data1["coordinates"]=json_data["place"]["bounding_box"]["coordinates"][0][0]
                    else:
                        if len(geo):
                                json_data1["coortinates"]=geo
                        else:
                            json_data1["coordinates"] =[]
                    json_data1["is_retweet"]=json_data["retweeted"]
                    json_data1['screen_name']=json_data['user']['screen_name']
                    json_data1["is_quote"]=json_data["is_quote_status"]
                    json_data1['text']=full_text
                    json_data1['retweet_count']=json_data['retweet_count']
# Converting dates to saudd time
                    utc=datetime.datetime.strptime(json_data['created_at'],'%a %b %d %H:%M:%S +0000 %Y') 
                    tzz=pytz.timezone('Asia/Riyadh')
                    utc = utc.replace(tzinfo=pytz.utc).astimezone(tzz)
                    json_data1['created_at']=utc    
                    json_data1['timestamp_ms']=datetime.datetime.timestamp(json_data1['created_at'])*1000
# This is used to create an entry in the index of elasticsearch. This will be used for searching inside the projects
                    t=tweets(Project_id=json_data1['Project_id'],text=json_data1['text'],cleaned_text=json_data1["cleaned_text"],hashtags=json_data1["hashtags"],
                    retweet_screen_name=json_data1['retweet_screen_name'],mentions_screen_name=json_data1["mentions_screen_name"],quoted_screen_name=json_data1['quoted_screen_name'],
                    sentiment_analysis=json_data1['sentiment_analysis'],status_id=json_data1['status_id'],profile_image_url_https=json_data["user"]["profile_image_url_https"],
                    is_retweet=json_data1['is_retweet'],is_quote=json_data1['is_quote'],created_at=json_data1['created_at'],timestamp_ms=json_data1['timestamp_ms'],screen_name=json_data1['screen_name'],
                    coordinates=json_data1['coordinates'],retweet_count=json_data1['retweet_count'],type=json_data1['type'],probability=prob,id_str=json_data["user"]["id_str"]) 
                    t.save()
                    time=db.project_lastcount.find_one({"access_token":access})
                    now = datetime.datetime.now()
                    if time['Time']+datetime.timedelta(hours=1)<now:
                           t={}
                           t['count']=time['count']
                           t['Time']=time['Time']
                           t['access_token']=time['access_token']
                           db.project_hourlycount.insert(t)
                           Time=now.replace(minute=0,second=0)
                           db.project_lastcount.update_one({"access_token":time['access_token']},{"$set":{"count":0,"Time":Time}})
                    else:
                        db.project_lastcount.update_one({"access_token":access},{"$set":{"count":time["count"]+1}})

        except Exception as e:
            st='An Error has occured: '+datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")+" "+str(e)
            db.project_tokens.update_one({"access_token":access},{"$set":{"last_error":st}})

# Compute account's probability of being a bot
def ComputeAccountProbability(tweet):
    prob=-1
    s=TweetsDocument.search().filter("term",id_str=tweet["user"]["id_str"])
    check=False
# Check if user is already listed in the db or not
    for hit in s:
             check=True
             prob=hit.probability
             break
    if prob==-1:
# If user is not listed then calculate is probability and store in the db
       if check:
           prob=ComputebotTwitterProbability(tweet)
           twts=tweets.objects.filter(id_str=tweet["user"]["id_str"]).update(probability=prob)
    return prob


# Computing the account probability
def ComputebotTwitterProbability(tweet):
    P=0
    Sources = ["Quran.to", "du3a.org", "athkarapp.online", "ShiaPrayersQ8.com", "AthanTweets.com", "swarmapp.com"]
    for url in tweet['entities']['urls']:
        if any(Source in url['display_url']  for Source in Sources):
                    P=1
                    return P
# If the source is not verified and source is not from iphone or android then p=1 otherwise check number of hashtags
    if not tweet["user"]["verified"] and not (re.search('iPhone',tweet["source"]) or  re.search('Android',tweet["source"])):
        if len(tweet["entities"]["urls"]):
              P=1
        else:
              P=0.85
    elif len(tweet["entities"]["hashtags"])>=6:
        P=1
    elif (len(tweet["entities"]["hashtags"])>=4 and len(tweet["entities"]["hashtags"])<=5):
        P=0.75
    elif (len(tweet["entities"]["hashtags"])==3):
        P=0.5
    return P

