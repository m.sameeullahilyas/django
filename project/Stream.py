#!/usr/bin/env python
#encoding: utf - 8
"""
@author: Muhammad Samiullah Ilyas
"""
import tweepy
from project.models import tweets
from visualize.documents import TweetsDocument
import json
import re
import time
import pytz
from .sentiment_analysis import calculate_sentiment
from . import views as v
import datetime
from dateutil import tz



# Inherit from the StreamListener object
class MyStreamListener(tweepy.StreamListener):
    db = None
    keywords=None
    a=True
    project_id=1
    access=None
    def __init__(self):
        super(MyStreamListener, self).__init__()
#Called once connected to streaming server
    def on_connect(self): 
        print("You are now connected to the streaming API.")

# Error handling
    def on_error(self, status_code):
        str='An Error has occured: ' + repr(status_code) + " "+datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        self.db.project_tokens.update_one({"access_token":self.access},{"$set":{"last_error":str}})    
        time.sleep(10)
        return False

    def on_exception(self, exception):
        str = 'An Exception has occured: ' + repr(exception) + " "+datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        self.db.project_tokens.update_one({"access_token":self.access},{"$set":{"last_error":str}})    
        return False

# Timeout handling
    def on_timeout(self):
         # Print timeout message
        str='Timeout...'
        # Wait 10 sec
        time.sleep(10)
        self.db.project_tokens.update_one({"access_token":access_token},{"$set":{"last_error":str}})     
        # Return nothing
        return True

#Overload the on_data method
    def on_data(self, data):
        try:
            json_data1={}
            json_data = json.loads(data) 
# Check if the data is a tweet or not
            if 'delete' and 'event'and 'direct_message'and 'friends' and 'limit' and 'disconnect' and 'warning' not in json_data:                         
# Check if location is given or not. If location is given then keywords parameter is set. Check if the tweet contains the required keywords
                  if self.keywords is not None:
                          self.a=False
                          key="|".join(self.keywords)
                          if json_data['truncated'] is True:						  
                             if re.search(key,json_data["extended_tweet"]['full_text']):
                                    self.a=True
                          if re.search(key,json_data['text']):
                                 self.a=True
                          if json_data['retweeted']	is True:
                              if json_data['retweeted_status']['truncated'] is True:						  
                                 if re.search(key,json_data['retweeted_status']["extended_tweet"]['full_text']):
                                        self.a=True
                              if re.search(key,json_data['retweeted_status']['text']):
                                 self.a=True
                              if re.search(key,json_data['retweeted_status']['user']['screen_name']) or re.search(key,json_data['retweeted_status']['user']['name']):
                                        self.a=True

                          if json_data['is_quote_status'] is True and 'quoted_status' in json_data:
                              if json_data['quoted_status']['truncated'] is True:						  
                                 if re.search(key,json_data['quoted_status']["extended_tweet"]['full_text']):
                                        self.a=True
                              if re.search(key,json_data['quoted_status']['text']):
                                 self.a=True
                              if re.search(key,json_data['quoted_status']['user']['screen_name']) or re.search(key,json_data['quoted_status']['user']['name']):
                                        self.a=True
                          if re.search(key,json_data['user']['screen_name']) or re.search(key,json_data['user']['name']):
                                        self.a=True
# Calculate each tweet user's bot probability and see if tweets contained the required keywords or nor if location is specified
                  if(self.a is True):
# Calculate sentiment and also get cleaned text. If quoted a retweet then original otherwise retweet. If a tweet is a retweet then text is of retweet 
# and sentiment is also calculated from retweet. Same applies for quote. If retweet is of quote then original not retweet is taken for sentiment analysis
# Same applies for a quote that is of retweet.
                    prob=ComputeAccountProbability(json_data)
                    if json_data["retweeted"] is True:
                        if not json_data['retweeted_status']["is_quote_status"]:
                             if json_data['retweeted_status']['truncated'] is True: #(1) added comment tweet    
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['retweeted_status']['extended_tweet']['full_text'])
                                 full_text=json_data['retweeted_status']['extended_tweet']['full_text']
                             else:
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['retweeted_status']['text'])
                                 full_text=json_data['retweeted_status']['text']

                        else:
                             if json_data['truncated'] is True: #(1) added comment tweet
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['extended_tweet']['full_text'])
                                 full_text=json_data['extended_tweet']['full_text']
                             else:
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['text'])
                                 full_text=json_data['text']

                    elif json_data['is_quote_status'] is True and 'quoted_status' in json_data: 
                        if not json_data['quoted_status']["retweeted"]:
                             if json_data['quoted_status']['truncated'] is True: #(1) added comment tweet    
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['quoted_status']['extended_tweet']['full_text'])
                                 full_text=json_data['quoted_status']['extended_tweet']['full_text']
                             else:
                                 text,sentiment=calculate_sentiment(json_data['lang'],json_data['quoted_status']['text'])                          
                                 full_text=json_data['quoted_status']['text']
                        else:
                             if json_data['truncated'] is True: #(1) added comment tweet    
                                text,sentiment=calculate_sentiment(json_data['lang'],json_data['extended_tweet']['full_text'])
                                full_text=json_data['extended_tweet']['full_text']
                             else:
                                text,sentiment=calculate_sentiment(json_data['lang'],json_data['text'])                          
                                full_text=json_data['text']
                    else:
                        if json_data['truncated'] is True: #(1) added comment tweet
                            text,sentiment=calculate_sentiment(json_data['lang'],json_data['extended_tweet']['full_text'])
                            full_text=json_data['extended_tweet']['full_text']
                        else:
                            text,sentiment=calculate_sentiment(json_data['lang'],json_data['text'])
                            full_text=json_data['text']

# Save the parameters that are required for visualizing the data. Some of the data are used by the network analysis tool. Their name and structure
# are used in the way so that they can be used in the network analysis tool. Hashtags and mentions are list of strings. Type is entered so that tweets
# can be categorized in the tweet feed accordingly. 
                    json_data1['sentiment_analysis']=sentiment
                    json_data1['Project_id']=self.project_id
                    json_data1["text"]=full_text
                    json_data1["cleaned_text"]=text.split(" ")
                    json_data1['status_id']=json_data['id_str']
                    json_data1["hashtags"]=[]
                    json_data1["mentions_screen_name"]=[]
                    json_data1["retweet_screen_name"]=[]
                    json_data1["quoted_screen_name"]=[]
# Saving the hastags, mentions involved in the tweet. If retweet then hashtags and mentions will also be included in the retweet status, Same applies for
# Quoted tweet
                    if json_data["retweeted"] is True:
                        for hashtags in json_data['retweeted_status']['entities']['hashtags']:
                                 json_data1["hashtags"].append(hashtags["text"])
                        for mentions in json_data['retweeted_status']['entities']['user_mentions']:
                                     json_data1["mentions_screen_name"].append(mentions["screen_name"])

                    if json_data['is_quote_status'] is True and 'quoted_status' in json_data:
                        for hashtags in json_data['quoted_status']['entities']['hashtags']:
                                 json_data1["hashtags"].append(hashtags["text"])
                        for mentions in json_data['quoted_status']['entities']['user_mentions']:
                                 json_data1["mentions_screen_name"].append(mentions["screen_name"])								 
                    
                    if json_data['truncated'] is True:
                        for hashtags in json_data['extended_tweet']['entities']['hashtags']:
                                 json_data1["hashtags"].append(hashtags["text"])
                        for mentions in json_data['extended_tweet']['entities']['user_mentions']:
                                 json_data1["mentions_screen_name"].append(mentions["screen_name"])
                       
                    for hashtags in json_data['entities']['hashtags']:
                             json_data1["hashtags"].append(hashtags["text"])
                    for mentions in json_data['entities']['user_mentions']:
                             json_data1["mentions_screen_name"].append(mentions["screen_name"])

                    if "media" in json_data['entities']:
                        for media in json_data['entities']['media']:
                            if media['type']=='photo' or media['type']=='video':
                                 json_data1["type"]=media['type']
                            else:
                                 json_data1['type']="other"
                    elif json_data['truncated'] is True:
                       if len(json_data['extended_tweet']['entities']['urls']):
                        for urls in json_data['extended_tweet']['entities']['urls']:
                             json_data1["type"]="Links"                        
                       if len(json_data['entities']['urls']):
                        for urls in json_data['entities']['urls']:
                             json_data1["type"]="Links"                        					
                    elif len(json_data['entities']['urls']):
                        for urls in json_data['entities']['urls']:
                             json_data1["type"]="Links"                        
                    else:
                        json_data1['type']="other"						                    
                   
                    if json_data["retweeted"] is True:
                           json_data1["retweet_screen_name"]=json_data["retweeted_status"]["user"]["screen_name"]
                    if json_data["is_quote_status"] is True and "quoted_status" in json_data:
                           json_data1["quoted_screen_name"]=json_data["quoted_status"]["user"]["screen_name"]
                    
                    if(len(json_data1["retweet_screen_name"])==0):
                            json_data1["retweet_screen_name"]=""
                    
                    if(len(json_data1["quoted_screen_name"])==0):
                            json_data1["quoted_screen_name"]=""   
       
                    if(json_data["coordinates"]):
                        json_data1["coordinates"]=[]
                        json_data1["coordinates"].extend([json_data["coordinates"]["coordinates"][0],json_data["coordinates"]["coordinates"][1]])
                    elif(json_data["place"]):
                        json_data1["coordinates"]=json_data["place"]["bounding_box"]["coordinates"][0][0]
                    else:
                        json_data1["coordinates"] =[]
                    json_data1["is_retweet"]=json_data["retweeted"]
                    json_data1['screen_name']=json_data['user']['screen_name']
                    json_data1["is_quote"]=json_data["is_quote_status"]
                    json_data1['text']=json_data["text"]
                    json_data1['retweet_count']=json_data['retweet_count']
# Converting dates to saudd time
                    utc=datetime.datetime.strptime(json_data['created_at'],'%a %b %d %H:%M:%S +0000 %Y') 
                    tzz=pytz.timezone('Asia/Riyadh')
                    utc = utc.replace(tzinfo=pytz.utc).astimezone(tzz)
                    json_data1['created_at']=utc	
                    json_data1['timestamp_ms']=(int(int(json_data['timestamp_ms'])/1000)+10800)*1000
# This is used to create an entry in the index of elasticsearch. This will be used for searching inside the projects
                    t=tweets(Project_id=json_data1['Project_id'],text=json_data1['text'],cleaned_text=json_data1["cleaned_text"],hashtags=json_data1["hashtags"],
                    retweet_screen_name=json_data1['retweet_screen_name'],mentions_screen_name=json_data1["mentions_screen_name"],quoted_screen_name=json_data1['quoted_screen_name'],
                    sentiment_analysis=json_data1['sentiment_analysis'],status_id=json_data1['status_id'],profile_image_url_https=json_data["user"]["profile_image_url_https"],
                    is_retweet=json_data1['is_retweet'],is_quote=json_data1['is_quote'],created_at=json_data1['created_at'],timestamp_ms=json_data1['timestamp_ms'],screen_name=json_data1['screen_name'],
                    coordinates=json_data1['coordinates'],retweet_count=json_data1['retweet_count'],type=json_data1['type'],probability=prob,id_str=json_data["user"]["id_str"]) 
                    t.save()
                    time=self.db.project_lastcount.find_one({"access_token":self.access})
                    now = datetime.datetime.now()
                    if time['Time']+datetime.timedelta(hours=1)<now:
                           t={}
                           t['count']=time['count']
                           t['Time']=time['Time']
                           t['access_token']=time['access_token']
                           self.db.project_hourlycount.insert(t)
                           Time=now.replace(minute=0,second=0)
                           self.db.project_lastcount.update_one({"access_token":time['access_token']},{"$set":{"count":0,"Time":Time}})
                    else:
                        self.db.project_lastcount.update_one({"access_token":self.access},{"$set":{"count":time["count"]+1}})
            return True

        except Exception as e:
            st='An Error has occured: '+datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")+" "+str(e)
            self.db.project_tokens.update_one({"access_token":self.access},{"$set":{"last_error":st}})
            return  True

# Starting the stream
def start_stream(stream, track,language,id,follow,coordinates,access_token,db):
    try :
        stream.listener.db=db
        stream.listener.project_id=id
        stream.listener.access=access_token

# Check if coordinates are given or not. If yes then place the keywords inside the listener objects keyword variable
        if (coordinates is not None):
             stream.listener.keywords=track
             stream.filter(locations=coordinates,languages=[language],is_async=False)

        else :
             if follow is not None:
                 stream.filter(track=track, languages=[language],follow=follow,is_async=False)
             else:
                 stream.filter(track=track, languages=[language],is_async=False)
    except Exception as e:
        print(e) 
        stream.disconnect()
        time.sleep(10)
# Starting the projects. First see if there is a token that is being used by only 1 project. If not then search for a new token        
def start_project(keywords,language,id,follow,coordinates,token,db):  
   		 	

   # Variables that contains the user credentials to access Twitter API
    ACCESS_TOKEN = token['access_token']
    ACCESS_TOKEN_SECRET = token['access_key']
    CONSUMER_KEY = token['consumer_key']
    CONSUMER_SECRET = token['consumer_secret']

#create an OAuthHandler instance and pass the consumer token and secret
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
# Create a wrapper for the API provided by Twitter
    api = tweepy.API(auth,wait_on_rate_limit=True,wait_on_rate_limit_notify=True)
    # Add your wrapper and your listener to the stream object
    mylistener=MyStreamListener()
    myStream = tweepy.Stream(auth=api.auth, listener=mylistener)
    start_stream(myStream, keywords,language,id,follow,coordinates,ACCESS_TOKEN,db)
# Compute account's probability of being a bot
def ComputeAccountProbability(tweet):
    prob=-1
    s=TweetsDocument.search().filter("term",id_str=tweet["user"]["id_str"])
    check=False
# Check if user is already listed in the db or not
    for hit in s:
             check=True
             prob=hit.probability
             break
    if prob==-1:
# If user is not listed then calculate is probability and store in the db
       if check:
           prob=ComputebotTwitterProbability(tweet)
           twts=tweets.objects.filter(id_str=tweet["user"]["id_str"]).update(probability=prob)
    return prob


# Computing the account probability
def ComputebotTwitterProbability(tweet):
    P=0
    Sources = ["Quran.to", "du3a.org", "athkarapp.online", "ShiaPrayersQ8.com", "AthanTweets.com", "swarmapp.com"]
    for url in tweet['entities']['urls']:
        if any(Source in url['display_url']  for Source in Sources):
                    P=1
                    return P
# If the source is not verified and source is not from iphone or android then p=1 otherwise check number of hashtags
    if not tweet["user"]["verified"] and not (re.search('iPhone',tweet["source"]) or  re.search('Android',tweet["source"])):
        if len(tweet["entities"]["urls"]):
              P=1
        else:
              P=0.85
    elif len(tweet["entities"]["hashtags"])>=6:
        P=1
    elif (len(tweet["entities"]["hashtags"])>=4 and len(tweet["entities"]["hashtags"])<=5):
        P=0.75
    elif (len(tweet["entities"]["hashtags"])==3):
        P=0.5
    return P
