# Configuration for the admin panel. Contains settings for User model i.e the page that will appear at 34.66.198.6/admin/login

from django.contrib import admin
from .models import user
from django.shortcuts import redirect
from django.utils.html import format_html
from django.contrib.auth.models import Group
from django.urls import reverse
from django.urls import re_path
from project.models import project
from project.views import deletee
# Register your models here.
class userAdmin(admin.ModelAdmin):
 # The options to be displayed when login of admin panel is opened. actions_html, actions_html1 and actions_html2 are buttons used for activatation, email verification 
 # and deletion of user respectively
 list_display=['first_name','last_name','phone','email','is_active','is_verified','actions_html','actions_html1','actions_html2']
 # Fields to search for in the search bar placed in the login admin panel
 search_fields = ['first_name', 'last_name', 'email']
 # Function that maps url to admin functions. Works in the same way as urls.py placed in Twitter/ directory. The urls defined here will be used by reverse
 # below to get the required url for each button. Each url is assigned a name and on calling the name the required url will be fetched.
 def get_urls(self):
        urls = super().get_urls()
# ObjectId is a unique parameter for each user. It is assigned by MongoDb to each of its entries and will be used to identify which user is being 
# referenced in the call to activate, verify or delete
        custom_urls = [
            re_path(
                r'^active/(?P<ObjectId>[a-zA-Z0-9]+)$',
                 self.admin_site.admin_view(self.active),
                name='active',
            ),
			re_path(
                r'^verify/(?P<ObjectId>[a-zA-Z0-9]+)$',
                 self.admin_site.admin_view(self.verify),
                name='verify',
            ),
			re_path(
                r'^deleteuser/(?P<ObjectId>[a-zA-Z0-9]+)$',
                 self.admin_site.admin_view(self.deleteuser),
                name='deleteuser',
            ),
        ]
        return custom_urls + urls

# Function to render each activate button in a row. This function acts as HTML and CSS for each button. The reverse function will generate the onclick or href parameter
# for each button. The admin::active is the function that will be called when button is clicked and arguments passed to the function will be the id of the
# user at each row. The id is required to generate the correct url i.e active/1 where 1 is the id of the user that is to be activated or deactivated. Each id is a unique parameter
# for the user
 def actions_html(self, obj):
# If the user is activated already then show the text on the button Deactivate otherwise show Activate
        if(obj.is_active):
            return format_html('<a class="button"  href={}  type="button">Deactivate</button>',reverse('admin:active', args=[obj._id]))
        else:
            return format_html('<a class="button"  href={} type="button">Activate</button>',reverse('admin:active', args=[obj._id]))

# Function to render each Verify button in a row. This function acts as HTML and CSS for each button. The reverse function will generate the onclick or href parameter
# for each button. The admin::verify is the function that will be called when button is clicked and arguments passed to the function will be the id of the
# user at each row. The id is required to generate the correct url i.e verify/1 where 1 is the id of the user that is to be verified or Deverified. Each id is a unique parameter
# for the user
 def actions_html1(self, obj):
 # If the user email is verified already then show the text on the button DeVerify otherwise show Verified
        if(obj.is_verified):
            return format_html('<a class="button"  href={}  type="button">DeVerify</button>',reverse('admin:verify', args=[obj._id]))
        else:
            return format_html('<a class="button"  href={} type="button">Verify</button>',reverse('admin:verify', args=[obj._id]))

# Function to render each Delete user button in a row. This function acts as HTML and CSS for each button. The reverse function will generate the onclick or href parameter
# for each button. The admin::deleteuser is the function that will be called when button is clicked and arguments passed to the function will be the id of the
# user at each row. The id is required to generate the correct url i.e deleteuser/1 where 1 is the id of the user that is to be deleted. Each id is a unique parameter
# for the user
 def actions_html2(self, obj):
    return format_html('<a class="button"  href={}  type="button">Delete</button>',reverse('admin:deleteuser', args=[obj._id]))			
# Must allow html tags otherwisse buttons will not be rendered
 actions_html.allow_tags = True
 actions_html1.allow_tags = True
 actions_html2.allow_tags = True
# Text to show on top bar of the Table that is listing all the users. Acts as <th> parameter for the new buttons defined whereas the buttons listed act as
# <td> parameters
 actions_html.short_description = "Acttivate/Deactivate"
 actions_html1.short_description = "Verify"
 actions_html2.short_description = "Delete User"
# Function that will activate/deactivate the users. Users are identified by their unique ObjectId
 def active(self, request, ObjectId, *args, **kwargs):
# if user is already activated then deactivate it otherwise activate it. If user is already activated then the text that was displayed on the button will be
# deactivate, so, deactivation of the user will take place    
    users=  user.objects.get(_id=ObjectId)  
    if (users.is_active):
         users.is_active=False
    else:
         users.is_active=True
    users.save()
# Redirecting to the page listing the users and now will see the updated user with the parameter of activation changed	
    return redirect('../../user/')
 def verify(self, request, ObjectId, *args, **kwargs):
# if user is already verified then deverify it otherwise verify it. If user is already verified then the text that was displayed on the button will be
# deactivate, so, deactivation of the user will take place        
    users=  user.objects.get(_id=ObjectId)  
    if (users.is_verified):
         users.is_verified=False
    else:
         users.is_verified=True
    users.save() 
# Redirecting to the page listing the users and now will see the updated user with the parameter of verify changed	
    return redirect('../../user/')
 def deleteuser(self, request, ObjectId, *args, **kwargs):   
# Deleting a user will also delete all of the projects of the user. That is why Pymongo is imported and will be used to delete all the projects of that user.
    user_delete = user.objects.get(_id=ObjectId)
    delete_project=project.objects.filter(user=user_delete.email) 
    for delete in delete_project:    
              deletee(delete.id)
    delete_project.delete()
    user_delete.delete()
    return redirect('../../user/')

# The text to be displayed when logged in on the admin panel
admin.site.site_header = 'Admin Dashboard'
# Must register the model with the admin model so that the admin panel will show the new admin model and all of the things that are configured here.
admin.site.register(user,userAdmin)
# Unregistered Django's Group admin model so that it won't show up in the admin panel
admin.site.unregister(Group)
