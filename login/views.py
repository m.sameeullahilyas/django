# This page contains functions that are used in the login, register, forgot and reset page. It handles all the user login/registration functions
from django.shortcuts import render,redirect
from .models import user
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.core.mail import EmailMessage
from django.conf import settings
import urllib
import json

# Function to activate the user if the link given to it on its email is opened. This will activate the user by converting the token given to it
# to its respective ObjectId that is stored in the Db.
def activate(request, uidb64, token):
# If a user is already logged in then make it redirect to index.html page.
    if ('user' in request.session) and ('pass' in request.session):
        try:
            user.objects.get(email=request.session['user'],password=request.session['pass'])
            request.session['success_msg']= "You are already logged in"
            return redirect('/')

        except user.DosesNotExist:
             request.session.flush()
# Convert the token given in the email to the respective objectid and get the user from the Id. Change the verified parameter to true and save it. Then
# redirect the user to the login page so that user can now login.    
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        users = user.objects.get(_id=uid)
    except(TypeError, ValueError, OverflowError, user.DoesNotExist):
        users = None
    if users is not None and account_activation_token.check_token(users, token):
        users.is_verified = True
        users.save()
        # return redirect('home')
        request.session['success_msg']="Your email is verified and now you can use your account"
        return redirect('/login')
    else:
        request.session['error_msg']="Activation link is invalid"
        return redirect('/login')
# Function to register the user and save its entry in the Db
def register(request):
# If user is already logged in then redirect to the main page,
    if ('user' in request.session) and ('pass' in request.session):
        try:
            user.objects.get(email=request.session['user'],password=request.session['pass'])
            request.session['success_msg']= "You are already logged in"
            return redirect('/')

        except user.DoesNotExist:
             request.session.flush()
    context={}
# If method is POST then save the parameters inside the Db and new user will be created
    if request.method =="POST":
        ''' Begin reCAPTCHA validation '''
        recaptcha_response = request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode()
        req =  urllib.request.Request(url, data=data)
        response = urllib.request.urlopen(req)
        result = json.loads(response.read().decode())
        ''' End reCAPTCHA validation '''

        if not result['success']:
            return render(request,'register.html',{'error_msg':"Invalid Captcha. Please Try Again"})
# Check if email is already registered or not. If yes then display the error msg
        try:
            user.objects.get(email=request.POST.get('email').lower())
            context={
			 "error_msg":"A user is already registered with the given email"
			 }
# If phone is listed then place the parameter inside otherwise don't			 
        except user.DoesNotExist: 
            if request.POST.get('Phone'):		
                user_create=user(
		password=(request.POST.get('Password')),
		first_name=request.POST.get('First Name'),
		last_name=request.POST.get('Last Name'),
		phone=request.POST.get('Phone'),
		email=request.POST.get('email').lower(),
		is_active = True,
		is_verified = False
		)
# Default parameters for Activate and email verify is True and False. When email given to the user is opened the is_verified changes to True
# and user will be able to login. When a new user is registered the activate parameter is True which can be changed by the admin to deactivate
# and as a result, the user will not be able to login.		
            else:
                user_create=user(
		password=(request.POST.get('Password')),
		first_name=request.POST.get('First Name'),
		last_name=request.POST.get('Last Name'),
		email=request.POST.get('email').lower(),
		is_active = True,
		is_verified = False
		)
# Create the user and send email to the user at the email entered here. The email will contain a token that is deriveed from the ObjectId
# of the user.
            try:
                user_create.save()
                new_user=user.objects.get(email=request.POST.get('email').lower())
                current_site = get_current_site(request)
                mail_subject = 'Activate your account.'
                message = render_to_string('acc_active_email.html', {
                'First_Name': new_user.first_name,
                'Last_Name': new_user.last_name,
                'Phone#': new_user.phone,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(new_user._id)),
                'token':account_activation_token.make_token(new_user),
            })

                toemail = new_user.email
                email = EmailMessage(
                        mail_subject, message, to=[toemail]
                )
                email.send()
                context={
				"success_msg":"Please check your email for verification"
				}
            except Exception as e:
                context={
	    "error_msg":e
	     }
        except Exception as e:
             context={
			 "error_msg":e
			 }
# Success msg will only be displayed if the success bar is placed inside the html templates
    if "success_msg" in context:
       request.session['success_msg']=context["success_msg"]
       return redirect('/login')
    else:
       return render(request,'register.html',context)
# Function to login in the user
def login(request):
# If user is already logged in then redirect to the main page
   if ('user' in request.session) and ('pass' in request.session):
        try:
            user.objects.get(email=request.session['user'],password=request.session['pass'])
            request.session['success_msg']= "Logged In successfully"
            return redirect('/')

        except user.DoesNotExist:
             request.session.flush()
# if method is post then check the entered parameters against those saved in the Db to see if a user exists and if the password entered is correct
# A user that is not verified will not be able to login and a user that is not activated will not be able to login. Different error msgs will be shown
# in different scenarios.
   context={}
   if request.method =="POST":
        ''' Begin reCAPTCHA validation '''
        recaptcha_response = request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode()
        req =  urllib.request.Request(url, data=data)
        response = urllib.request.urlopen(req)
        result = json.loads(response.read().decode())
        ''' End reCAPTCHA validation '''

        if not result['success']:
            return render(request,'login.html',{'error_msg':"Invalid Captcha. Please Try Again"})
        try:	
            user_login=user.objects.get(email=request.POST.get('email').lower())
            password=(request.POST.get('Password'))
            check=(password,user_login.password)
            if not check:
                     context={
					 "error_msg":"Password is wrong. Please try again."
					 }
            elif not user_login.is_active:
                     context={
	                      "error_msg":"Your account is deactivated. For further details contact a@example.com"
	                     }
            elif not user_login.is_verified:
                          context={
	                      "error_msg":"Your email is not verified please check your mail for verification. For further details contact a@example.com"
	                     }
            else:
                        request.session.flush()
                        request.session['user']=user_login.email
                        request.session['pass']=user_login.password
                        request.session['success_msg']="Log in Successful" 
                        return redirect('/')
        except user.DoesNotExist: 		       
            context={
	       "error_msg":"There is no such email registered"
	       } 
          

        except Exception as e:
            msg=e
            context={
	    "error_msg":msg
	     }   
   
   
   if 'success_msg' in request.session:
          context={
		  "success_msg":request.session['success_msg']
		  }
   elif 'error_msg' in request.session:
          context={
		  "error_msg":request.session['error_msg']
		  }
   request.session.flush()		  
   return render(request,'login.html',context)
# On clicking the logout option, session will be flushed and user will now be redirected to the login page 
def logout(request):
    if ('user' not in request.session) or ('pass' not in request.session):
        return redirect('/login')
    
    try:
        user.objects.get(email=request.session['user'],password=request.session['pass'])

    except user.DoesNotExist:
        return redirect('/login')
    request.session.flush()
    return redirect('/login')	
# On forgotting the password, the user will be sent an email to the email entered in the field. This email will be checked whether it belongs to a@example
# user or not. If not then error msg will be displayed otherwise an email is sent to the given email which will contain a link to the reset password page for
# that user.
def forgot(request):
   if ('user' in request.session) and ('pass' in request.session):
        try:
            user.objects.get(email=request.session['user'],password=request.session['pass'])
            request.session['success_msg']= "You are already logged in"
            return redirect('/')

        except user.DoesNotExist:
             request.session.flush()   
   context={}
   if request.method =="POST":          
        try:
            forgot_user = user.objects.get(email=request.POST.get('email').lower())
            current_site = get_current_site(request)
            mail_subject = 'Reset Your Password.'
            message = render_to_string('acc_forgot_password.html', {
                'First_Name': forgot_user.first_name,
                'Last_Name': forgot_user.last_name,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(forgot_user._id)),
                'token':account_activation_token.make_token(forgot_user),
            })
			
            toemail = forgot_user.email
            email = EmailMessage(
                        mail_subject, message, to=[toemail]
                )
            email.send()
            context={
				"success_msg":"Please check your email for password reset"
				}    
       
        except user.DoesNotExist:   
           context={
		   "error_msg": "There is no such email registered"
		   }
             
        except Exception as e:
                context={
	    "error_msg":e
	     }   
           
   if "success_msg" in context:
       request.session['success_msg']=context["success_msg"]
       return redirect('/login')
   else:
       return render(request,'forgot.html',context)
# The forgot password email contains a link to the reset page. This link contains a token that is generated from the user objectid and on clicking the link
# the token is converted to the user id to see if the user exists or not. If reset page link is correct then user will be able to reset his password.
def reset(request, uidb64, token):
    if ('user' in request.session) and ('pass' in request.session):
        try:
            user.objects.get(email=request.session['user'],password=request.session['pass'])
            request.session['success_msg']= "You are already logged in"
            return redirect('/')

        except user.DoesNotExist:
             request.session.flush()   
    if request.method == "POST":
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            users = user.objects.get(_id=uid)
        except(TypeError, ValueError, OverflowError, user.DoesNotExist):
            users = None
        if users is not None and account_activation_token.check_token(users, token):
            users.password = (request.POST.get('Password'))
            users.save()
            request.session['success_msg']="Your password is resetted. Log in with new password"
            return redirect('/login')
        else:
            return render(request,'login.html',{"error_msg":"Reset link is invalid"})
    else:
        return render(request,'reset.html')

