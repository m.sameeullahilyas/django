# The model of the user i.e. the fields that will be stored inside the database for each user in the MongoDb
from django.db import models
from djongo import models as s

class user(models.Model):
    _id = s.ObjectIdField(db_index=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200,unique=True,db_index=True)
    phone = models.IntegerField()
    password = models.CharField(max_length=200,db_index=True)
    is_active = models.BooleanField()
    is_verified = models.BooleanField()
    class Meta:
       indexes = [
           models.Index(fields=['email','password','_id'])
		   ]
		   
		   
		   
# _id is the unique object id that will be generated by MongoDb, email is the unique field for each user and no user can register
# again with the same email. Phone is the integer field and cannot contain alphabets. Is activate and is verified are boolean and are used as 
# indicators to indicate whether a user/email of user is activated/verified or not. True will indicate that the user/email is activate/verified